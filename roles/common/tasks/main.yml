---
## This playbook installs and configures AD authentication

- name: Disable firewall
  service:
    name: firewalld
    state: stopped
    enabled: no
 
- name: create tmp directory
  file:
    path: /mnt/tmp
    state: directory
  ignore_errors: yes  
  
- name: subscribe to RedHat
  shell: /bin/bash -c "subscription-manager register --username Gstarraw --password L1nuxf0rl1fe --auto-attach --force"
  
- name: Mount Groups share - CentOS 7
  shell: /bin/bash -c "mount -t cifs //g-star.raw/data/install -o username=_svc_compadd,domain=G-STAR.RAW,password=R@wadd00 /mnt/tmp"
  when: ansible_distribution_major_version == "7"
  
- name: Mount Groups share - CentOS 6
  shell: /bin/bash -c "mount -t cifs //nl19fs002/install -o username=_svc_compadd,domain=G-STAR.RAW,password=R@wadd00 /mnt/tmp"
  when: ansible_distribution_major_version == "6"
  
- name: copy VMWare Tools Installation
  shell: /bin/bash -c "cp /mnt/tmp/Server/Linux/VMwareTools-9.0.15-2323214.tar.gz /tmp"
  ignore_errors: yes
  
- name: untar and run
  shell: /bin/bash -c "tar -zxvf /tmp/VMwareTools-9.0.15-2323214.tar.gz -C /tmp/"
  ignore_errors: yes
  
- name: Install some extra software for VMwareTools - CentOS 7
  yum:
   pkg: "{{ item }}"
   state: installed
  with_items:
    - perl
    - gcc
    - make
    - kernel-headers
    - kernel-devel
  when: ansible_distribution_major_version == "7"
  
- name: More software - CentOS 7
  shell: /bin/bash -c "yum -y -q install 'kernel-devel-uname-r == $(uname -r)'"
  when: ansible_distribution_major_version == "7"
  
- name: Run the VMWare installer
  shell: /bin/bash -c "/tmp/vmware-tools-distrib/vmware-install.pl -d default"
  
- name: Install Software needed to join domain - CentOS 7
  yum:
    pkg: "{{ item }}"
    state: installed
  with_items:
    - realmd
    - sssd
    - oddjob
    - oddjob-mkhomedir
    - adcli
    - samba-common
  when: ansible_distribution_major_version == "7"
  
- name: Install Software needed to join domain - CentOS 6
  yum:
    pkg: "{{ item }}"
    state: installed
  with_items:
    - authconfig
    - krb5-workstation
    - pam_krb5
    - samba-common
    - oddjob-mkhomedir
    - ntp
    - sudo
  when: ansible_distribution_major_version == "6"        
      
- name: Download Python Pip install script - CentOS 7
  shell: curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
  when: ansible_distribution_major_version == "7"
  
- name: Run Install script Python Pip - CentOS 7
  shell: python get-pip.py
  when: ansible_distribution_major_version == "7"  
  
- name: Run authconfig and set up the required settings and files - CentOS 6
  shell: /bin/bash -c "authconfig --disablecache --enablewinbind --enablewinbindauth --smbsecurity=ads --smbworkgroup=GSTARRAW --smbrealm=G-STAR.RAW --enablewinbindusedefaultdomain --winbindtemplatehomedir=/home/G-STAR.RAW/%U --winbindtemplateshell=/bin/bash --enablekrb5 --krb5realm=G-STAR.RAW --enablekrb5kdcdns --enablekrb5realmdns --enablelocauthorize --enablemkhomedir --enablepamaccess --updateall"
  when: ansible_distribution_major_version == "6"
  
- name: Install Pexpect using Pip - CentOS 7
  shell: /bin/bash -c "pip install pexpect"
  when: ansible_distribution_major_version == "7"
  
- name: Join system to Domain - CentOS 7
  expect:
    command: /bin/bash -c "/usr/sbin/realm join G-STAR.RAW --user=_svc_compadd"
    responses: 
      Password for _svc_compadd: "R@wadd00"
  when: ansible_distribution_major_version == "7"
  ignore_errors: yes

- name: join domain - CentOS 6
  shell: /bin/bash -c "net ads join g-star.raw -U _svc_compadd%R@wadd00"
  when: ansible_distribution_major_version == "6"  
  ignore_errors: yes
  
- name: mkdir /home/g-star.raw - CentOS 6
  file: 
    path: /home/g-star.raw
    state: directory
    owner: root
    group: root
    mode: 0777   
  when: ansible_distribution_major_version == "6"
   
- name: Deny all logins - CentOS 7
  shell: /bin/bash -c "realm deny --all"
  when: ansible_distribution_major_version == "7"
  
- name: Allow ADM group for this server - CentOS 7
  shell: realm permit -g G-STAR.RAW\\ADM-OMS-$HOSTNAME
  when: ansible_distribution_major_version == "7"
  
- name: Add group to sudoers - CentOS 7
  shell: echo "%ADM-OMS-$HOSTNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
  when: ansible_distribution_major_version == "7"
      
- name: Add group to sudoers - CentOS 6
  shell: echo "%rdp-$HOSTNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
  when: ansible_distribution_major_version == "6"  
  
- name: LineinFile change FQDN required for logins - CentOS 7
  lineinfile:
    destfile: /etc/sssd/sssd.conf
    regexp: '^use_fully_qualified_names = True'
    line: 'use_fully_qualified_names = False'
  when: ansible_distribution_major_version == "7"
  
- name: Update PAM sshd files - CentOS 6
  shell: |
    echo '#%PAM-1.0
    auth       required     pam_sepermit.so
    auth       required     pam_sepermit.so
    auth       sufficient   pam_winbind.so
    auth       include      system-auth
    account    required     pam_nologin.so
    account    include      password-auth
    account    include      system-auth
    password   include      password-auth
    password   include      system-auth
    # pam_selinux.so close should be the first session rule
    session    required     pam_selinux.so close
    session    required     pam_loginuid.so
    # pam_selinux.so open should only be followed by sessions to be executed in the user context
    session    required     pam_selinux.so open env_params
    session    required     pam_namespace.so
    session    optional     pam_keyinit.so force revoke
    session    include      password-auth
    session    include      system-auth' > /etc/pam.d/sshd
  when: ansible_distribution_major_version == "6"  

- name: Update PAM systemauth - CentOS 6
  shell: |
    echo "#%PAM-1.0 
    # This file is auto-generated. 
    # User changes will be destroyed the next time authconfig is run. 
    auth required pam_env.so 
    auth sufficient pam_unix.so nullok try_first_pass 
    auth requisite pam_succeed_if.so uid >= 500 quiet 
    auth sufficient pam_krb5.so use_first_pass 
    auth sufficient pam_winbind.so use_first_pass 
    auth required pam_deny.so
    account required pam_access.so 
    account required pam_unix.so broken_shadow 
    account [default=ignore success=1] pam_succeed_if.so uid < 16777216 quiet 
    account [default=bad success=ignore] pam_succeed_if.so user ingroup rdp-$HOSTNAME quiet 
    account sufficient pam_localuser.so 
    account sufficient pam_succeed_if.so uid < 500 quiet 
    account [default=bad success=ok user_unknown=ignore] pam_krb5.so 
    account [default=bad success=ok user_unknown=ignore] pam_winbind.so 
    account required pam_permit.so

    password requisite pam_cracklib.so try_first_pass retry=3 
    password sufficient pam_unix.so md5 shadow nullok try_first_pass use_authtok 
    password sufficient pam_krb5.so use_authtok 
    password sufficient pam_winbind.so use_authtok 
    password required pam_deny.so

    session optional pam_keyinit.so revoke 
    session required pam_limits.so 
    session optional pam_mkhomedir.so 
    session [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid 
    session required pam_unix.so 
    session optional pam_krb5.so 
    " > /etc/pam.d/system-auth
  when: ansible_distribution_major_version == "6"       
    
- name: restart sssd to process changes - CentOS 7
  service:
    name: sssd
    state: restarted
  when: ansible_distribution_major_version == "7"
      
- name: umount the group share
  shell: umount /mnt/tmp 
