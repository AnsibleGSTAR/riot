CREATE EXTENSION plproxy;
CREATE EXTENSION pldbgapi;
CREATE EXTENSION adminpack;
CREATE ROLE authenticator LOGIN
  ENCRYPTED PASSWORD 'md5d477e04c94a412f7534455bfae97544c'  -- password to match postgrest service script
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION; 
 -- Password insight15
 CREATE ROLE riot_report LOGIN
  ENCRYPTED PASSWORD 'md5aa9646f858e7e2ced69aabb4cd852a0b'
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
  -- Password Shirts16
CREATE ROLE riot_remote LOGIN
  ENCRYPTED PASSWORD 'md5aa9646f858e7e2ced69aabb4cd852a0b'
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
  -- Password Rfid123

CREATE ROLE riot_replication LOGIN
  ENCRYPTED PASSWORD 'md5835e3b289de80a098d531c6a42eae57e'
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE REPLICATION;
  -- Password Rfid123!
CREATE ROLE anonymous;
CREATE ROLE riot;
ALTER role riot with encrypted password 'insight15';
ALTER ROLE riot WITH LOGIN;
CREATE ROLE riot_app;
CREATE ROLE riot_pos;  
GRANT anonymous, riot_app, riot_pos TO authenticator;
CREATE SERVER masterdb FOREIGN DATA WRAPPER plproxy
 OPTIONS (connection_lifetime '1800',
 p0 'dbname=riot host=172.30.11.54', p1 'dbname=riot host=172.30.11.54' );
CREATE DATABASE riot 
CREATE USER MAPPING FOR PUBLIC SERVER masterdb;
CREATE USER MAPPING FOR riot SERVER masterdb
 OPTIONS (user 'riot_remote', password 'Rfid123');
CREATE USER MAPPING FOR riot_app SERVER masterdb
 OPTIONS (user 'riot_remote', password 'Rfid123');
CREATE USER MAPPING FOR riot_pos SERVER masterdb
 OPTIONS (user 'riot_remote', password 'Rfid123');
GRANT USAGE ON FOREIGN SERVER masterdb TO riot, riot_app, riot_pos;  
ALTER USER riot WITH SUPERUSER;
  